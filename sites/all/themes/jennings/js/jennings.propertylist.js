(function ($) {

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        listing = function(){
            var propertyUl = $('#js-property').find('ul'),
                numLis = propertyUl.find('li').length;

            propertyUl.addClass('cols-'+numLis);
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            listing();
        };

    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);