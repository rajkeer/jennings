(function ($) {

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        choose = function(){
            sectionChoose = $('#choose').find('li');

            sectionChoose.matchHeight();
        },

        links = function(){
            sectionChoose = $('#links').find('.linksHeight');

            if (Window.width() > 500){
                sectionChoose.matchHeight({
                    byRow:false
                });
            }
        },

        setHeights = function(){
            var wrap = $('#sitemap'),
                setH = wrap.find('.target').height(),
                scroll = wrap.find('.setHeight');

            scroll.height(setH);
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            choose();
            links();
        },
        Load = function(){
            setHeights();
        };
    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

     // When page is loaded
    Window.load(function() {
        Load();
    });

})(jQuery);