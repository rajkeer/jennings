(function ($) {

	// Homepage only

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),


        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            //heading();
        };

 

    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);