(function ($) {

	// Homepage only

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),
        body = $('body'),

        members = function(){
            var members = $('#members'),
                memberLink = members.find('a.open'),
                closeLink = members.find('a.member-close'),
                memberBg = $('#member-open');

            memberLink.on('click', function(){
                var This = $(this),
                    thisContent = This.siblings('.full'),
                    thisParent = thisContent.parent(),
                    nonLi = members.find('li.active'),
                    nonContent = nonLi.find('.full');

                nonLi.removeClass('active');
                nonContent.addClass('hidden');

                thisContent.toggleClass('hidden');
                thisParent.toggleClass('active');

                memberBg.fadeIn();

                return false;
            });

            closeLink.on('click', function(){
                var This = $(this),
                    thisContent = This.parent().parent();

                thisContent.fadeOut(400);

                memberBg.fadeOut(400);

                return false;
            });
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            members();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);