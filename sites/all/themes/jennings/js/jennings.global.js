(function ($) {

    /*
        1. Set everything up as variables, e.g:
     
            myFunction = function(){
                var saveEverythingAsVariables = $('#everything');

                ... jquery in here
            }
     
        2. Then add function to required launcher, e.g:

            Ready = function(){
                myfunction();
            }
    */

    var 
        // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        // Make all external links open in new window
        externalLinks = function(){
            var extLink = Wrapper.find('a:not([href^="http://lh.jennings.com"]):not([href^="#"]):not([href^="/"]):not([href^="http://www.jennings.co.uk"])');

            extLink.attr('target','_blank');
        },

        // Ajax page loading
        ajaxLoading = function(){
            if ($.support.pjax) {                
                Document.pjax('a:not(a[href^="http"],a[href^="mailto"],a[target="_blank"], #admin-menu-account a)', Wrapper);

                Document.on('pjax:clicked', function() {
                    Wrapper.addClass('animated fadeOutUp');
                    htmlBody.velocity("scroll", { duration: 350, easing: "ease-in-out" });
                });
            }
        },

        // Smooth scrolling
        smoothScroll = function(){
            Wrapper.on('click', '.scroll', function() {
                var $target = $(this.hash);
                $target.velocity("scroll", { duration: 300, offset: -60, easing: "ease-in-out" });
                return false;
            });
        },

        // Mobile nav
        mobileNav = function(){
            var menuToggle = $('#navToggle'),
                nav = $('#superfish-1'),
                body = $('body');
                
            menuToggle.on('click', function(){
                nav.slideToggle(250);
                $(this).toggleClass('active');
                body.toggleClass('nav-open');
                return false;
            });
        },

        // Image row
        imageRow = function(){
            var awRow = $('.image-row');

            if (Window.width() > 480){
                awRow.each(function(){
                    if ($(this).children('li').length == 2){
                        $(this).addClass('row-two');
                    }
                    if ($(this).children('li').length == 3){
                        $(this).addClass('row-three');
                    }
                });
            }
        },

        fbox = function(){
            $(".fancybox-img").fancybox({
                maxWidth    : '80%',
                //maxHeight   : '90%',
                fitToView   : false,
                //width       : '90%',
                //height      : '90%',
                autoSize    : true,
                closeClick  : true,
                openEffect  : 'none',
                closeEffect : 'none'
            });
        },

        cookies = function(){
            var notice = $('#cookie-notice'),
                btn = $('#pp-close'),
                cookie = Cookies.get('cookieNotice');

            btn.on('click', function(){
                Cookies.set('cookieNotice','showCookieNotice',{ expires: 30 });
                notice.hide();
                return false;
            });

            if (cookie == null){
                notice.show();
            }
        },


        // ---- 
        // Prep functions to run
        // ----

        Ready = function(){
            // cookies();
            externalLinks();
            ajaxLoading();
            smoothScroll();
            mobileNav();
            imageRow();
            fbox();
        },

        Load = function(){

        },

        Unload = function(){

        };

    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

    // When page is loaded
    Window.load(function() {
        Load();
    });

    // When leaving page
    Window.unload(function() {
        Unload();
    });

})(jQuery);