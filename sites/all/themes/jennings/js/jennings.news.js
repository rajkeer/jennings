(function ($) {

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        newsHide = function(){
            var newsUl = $('ul.news-listing'),
                status = 0;

            newsUl.each(function(){
                $(this).find('li:lt(3)').removeClass('hide').removeClass('toggleable');
            });


            newsUl.find('.toggle-link').text('More posts >');
            newsUl.on('click','.toggle-link', function(){
                if (status == 0){
                    
                    $(this).parent().siblings('.toggleable').slideDown().removeClass('hide');
                    $(this).text('Less posts >');
                    status = 1;
                    $(this).parent().parent().velocity("scroll", { duration: 300, offset: -140, easing: "ease-in-out" });
                } else {
                    $(this).parent().siblings('.toggleable').addClass('hide').slideUp();
                    $(this).text('More posts >');
                    status = 0;
                    $(this).parent().parent().velocity("scroll", { duration: 300, offset: -140, easing: "ease-in-out" });
                }
                return false;
            });
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            newsHide();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);