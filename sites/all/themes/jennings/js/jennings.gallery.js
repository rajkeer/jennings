(function ($) {

    // Homepage only

    var // General purpose vars
        htmlBody = $('html,body'),
        Window = $(window),
        Document = $(document),
        Wrapper = $('#wrapper'),

        gallery = function(){
            $(".fancybox").fancybox({
                maxWidth    : '80%',
                //maxHeight   : '90%',
                fitToView   : false,
                //width       : '90%',
                //height      : '90%',
                autoSize    : true,
                closeClick  : true,
                openEffect  : 'none',
                closeEffect : 'none'
            });
        },

        // ---- 
        // Prep functions to run
        // ---- 

        Ready = function(){
            gallery();
        };


    // ----
    // Run everything
    // ----

    // Document.ready
    $(function() {
        Ready();
    });

})(jQuery);