<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix default-template"<?php print $attributes; ?>>
<div class="main-img"> <img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-home-hero-min.jpg" width="100%" alt="" /> </div>


<div class="main-txt-top">
  <div class="width-1">
  <?php if(isset($content['body'])):?>
   <div class="content-body">
	<?php print render($content['body']);?>
	</div>
  <?php endif;?>

  <?php if(isset($content['field_page_blocks_rows'])):?>
   <div class="content-page_blocks_rows">
	<?php print render($content['field_page_blocks_rows']);?>
	</div>
  <?php endif;?>
  
   <?php /* ?>
    <h1>MAKE YOUR BUSINESS AT HOME WITH JENNINGS </h1>
    <div class="txt-1"> <strong>At Jennings we want you to feel at home from the start. Our offices and business<br>
      units come in all sizes to suit you, and our unbeatable Jennings welcome is always
      on offer. </strong>
      <p>At our flagship Monument Park site in Chalgrove and other great locations in South<br>
        Oxfordshire, our first job is to support you. With Jennings on your side, we know you’ll<br>
        find the perfect home for your business.<br>
        Take a look around, we’re sure you’ll love us when you get to know us. With facilities<br>
        to suit you, your business and your employees, join our strong supportive business<br>
        community and enjoy the benefits. </p>
      <div class="ul-1">
        <ul>
          <li> <i><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-home-personal.png" width="100%" alt="" /></i> <strong>Personal service &
            relationships</strong>
            <p>Our know-how supporting
              your business</p>
          </li>
          <li> <i><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-home-location.png" width="100%" alt="" /></i> <strong>Personal service &
            relationships</strong>
            <p>Our know-how supporting
              your business</p>
          </li>
          <li> <i><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-home-affordable.png" width="100%" alt="" /></i> <strong>Personal service &
            relationships</strong>
            <p>Our know-how supporting
              your business</p>
          </li>
          <li> <i><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-home-meeting.png" width="100%" alt="" /></i> <strong>Personal service &
            relationships</strong>
            <p>Our know-how supporting
              your business</p>
          </li>
          <li> <i><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-home-amenities.png" width="100%" alt="" /></i> <strong>Personal service &
            relationships</strong>
            <p>Our know-how supporting
              your business</p>
          </li>
          <li> <i><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-home-responsible.png" width="100%" alt="" /></i> <strong>Personal service &
            relationships</strong>
            <p>Our know-how supporting
              your business</p>
          </li>
        </ul>
      </div>
      <p>Over 30 years our reputation has grown for honesty, trust and fairness, so you can feel
        secure knowing your business is safe in our hands</p>
		
		
    </div>
	 <?php  */?>
  </div>
</div>


  <?php if(isset($content['field_page_blocks_rows'])):?>
	<div class="content-page-row-section">
		<?php print render($content['field_page_blocks_rows']);?>
	</div>
<?php endif;?>

 <?php if(isset($content['field_page_content_row'])):?>
  <div class="page_content_row-section"><?php print render($content['field_page_content_row']);?> 
  </div>
 <?php endif;?>
 
 <?php /* ?>
<div class="image-fold-left fold-bg1 image-fold">
  <div class="container-fluid">
    <div class="image-fold-box"><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-What's-on-min.jpg" width="100%" alt="" /></div>
    <div class="container clearfix">
      <div class="large-6 right">
        <div class="image-fold-text">
          <h1>Workspaces</h1>
          <p>Are you starting a new business and need to start small while
            you think big? Or are you growing fast and need more space in
            a convenient location? We have the perfect solution for you –
            choose from offices, workshops and business units in Chalgrove,
            Garsington, Berinsfield and Thame. </p>
          <div class="img-fold-a"><a href="#">LEARN MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="image-fold-right fold-bg2 image-fold">
  <div class="container-fluid">
    <div class="image-fold-box"><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-What's-on-min.jpg" width="100%" alt="" /></div>
    <div class="container clearfix">
      <div class="large-6 left">
        <div class="image-fold-text">
          <h1>About us</h1>
          <p>We want people to want to come to work. We do business
            differently, based on building lasting and meaningful
            relationships. Here to support you, we offer great advice, and
            our expert & friendly team will make sure you feel at home with
            Jennings. </p>
          <div class="img-fold-a"><a href="#">LEARN MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="image-fold-left fold-bg1 image-fold">
  <div class="container-fluid">
    <div class="image-fold-box"><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-What's-on-min.jpg" width="100%" alt="" /></div>
    <div class="container clearfix">
      <div class="large-6 right">
        <div class="image-fold-text">
          <h1>What’s on</h1>
          <p>Our events calendar tells a busy story. Get involved, get inspired
            and start making connections. From business networking to
            social events and charity fundraisers, it’s all going on.</p>
          <div class="img-fold-a"><a href="#">LEARN MORE <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php */ ?>

  <?php if(isset($content['field_page_feedback_rows'])):?>
	  <div class="homefeedback-section">
	<div class="homefeedback-box">
	<?php print render($content['field_page_feedback_rows']);?> 
	</div>
	</div>	
<?php endif;?>

 <?php if(isset($content['field_page_video_rows'])):?>
 <div class="video-section">
<div class="width-1">
<?php print render($content['field_page_video_rows']);?> 
</div>
</div>
 <?php endif;?>

</div>