<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: whether submission information should be displayed.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>
<?php if ($view_mode == 'teaser'): ?>



<?php else: ?>

 <div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>



  <div class="content clearfix"<?php print $content_attributes; ?>>
  
  
  
  
  

<div class="pg-hd-box">
<div class="container-fluid">
    <div class="hd-img-box"><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/Jennings-What's-on-min.jpg" width="100%" alt="" /></div>
    <div class="container clearfix">
      <div class="large-6 right">
        <div class="hd-text-box">
          <h1>BUSINESS
UNITS</h1>
          
        </div>
      </div>
    </div>
  </div>
</div>

<div class="workspaces-subtxt-top">
  <div class="width-4">
    <h1>MONUMENT PARK IN CHALGROVE </h1>
    <div class="txt-1"> <strong>Our main site and hub for activities at Monument Park in Chalgrove has a
wide variety of business and starter units, whether you’re just starting out or
need something bigger. We are home to over 80 businesses employing more
than 600 people. </strong>
      <p>Our friendly on-site team will help you find the right space and settle you in
quickly. And we love to keep it simple, so we have short & flexible leases –
meaning you aren’t tied in to lengthy or difficult contracts. Doesn’t that feel like
home? </p>
      
      
    </div>
  </div>
</div>


<div class="workspace-two-section">
 <div class="width-3">
   <div class="workspace-two-box">
     <h1>Why choose a business unit?</h1>
     <div class="clearfix">
      <div class="workspace-two-box-l">
       <p>Our licenses are simple, flexible and designed to give you peace
of mind. If you soon find you need extra space, we’ll also work
with you to help accommodate your growing team.
</p>
<strong>Key benefits: </strong>
<ul>
  <li>Sizes to suit any business, from 300-4,000 sq. ft</li>
  <li>Versatile uses, with offices, workshops, manufacturing and
warehouses</li>
  <li>Sizes to suit any business, from 300-4,000 sq. ft</li>
  <li>Sizes to suit any business, from 300-4,000 sq. ft</li>
</ul>
<div class="button-1"><a href="#">EXPLORE THE PARK ></a></div>
      </div>
      <div class="workspace-two-box-r">
        <img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/workspace-img.jpg" width="100%" alt="" />
      </div>
     </div>
   </div>
 </div>
</div>







<div class="video-section">
<div class="width-1">
 <h1>A little flavour of Jennings </h1>
 <div class="video-box"><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/video.jpg" width="100%" alt=""></div>
</div>
</div>

<div class="homefeedback-section">
<div class="homefeedback-box">
<strong>“Monument Park … is the blueprint for a successful business community.
Part of the Jennings family, the friendly team … take a personal and active
interest in their customers.” </strong><i>DAVID, BIDWELLS</i>
</div>
</div>

<div class="call-us-section">
<div class="container">
 <h1>WANT TO FIND OUT MORE?</h1>
 <ul>
  <li><a href="#">01865 893200</a></li>
  <li><a href="#">hello@jennings.co.uk</a></li>
 </ul>
</div>
</div>
  
  <div class="image-fold-left fold-bg4 image-fold">
  <div class="container-fluid">
    <div class="image-fold-box"><img src="http://54.255.217.113/jennings/sites/all/themes/jennings/images/img-1.jpg" width="100%" alt="" /></div>
    <div class="container clearfix">
      <div class="large-6 right business-locations">
        <div class="image-fold-text">
          <h1>Other business locations</h1>
          <p>As well as Monument Park in Chalgrove we also have
work spaces available in three other locations: 
</p>
<ul>
 <li><strong>THE BOUNDARY, GARSINGTON</strong> A collection of workshops and offices, set in the<br>
beautiful village of Garsington, South Oxfordshire.</li>
 <li><strong>TOWER PARK, BERINSFORD
</strong> A selection of larger warehouse units, perfect for<br>
storage and manufacturing purposes. Home to<br>
Abingdon Gymnastics Club. </li>
 <li><strong>RETAIL SPACES, THAME</strong> A diverse group of retail spaces, within the busy<br>
market town centre.</li>

</ul>
  <p>Get in touch to discuss what you need <b>01865 893200</b>
</p>        
        </div>
      </div>
    </div>
  </div>
</div>
  

  <?php /*?>  <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?><?php */?>
  </div>

 


</div> 

<?php endif; ?>