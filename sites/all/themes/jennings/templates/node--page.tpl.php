<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix default-template"<?php print $attributes; ?>>

  <?php if(isset($content['field_page_banner_rows'])):?>
	<div class="content-main-banner-section">
		<?php print render($content['field_page_banner_rows']);?>
	</div>
<?php endif;?>

<div class="main-txt-top">

  <div class="width-1">
      <?php if(isset($content['body'])):?>
   <div class="content-body">
	<?php print render($content['body']);?>
	</div>
  <?php endif;?>
	</div>
</div>

  <?php if(isset($content['field_page_blocks_rows'])):?>
	<div class="content-page-row-section">
		<?php print render($content['field_page_blocks_rows']);?>
	</div>
<?php endif;?>

 <?php if(isset($content['field_page_content_row'])):?>
  <div class="page_content_row-section"><?php print render($content['field_page_content_row']);?> 
  </div>
 <?php endif;?>
 
  <?php if(isset($content['field_page_feedback_rows'])):?>
	  <div class="homefeedback-section">
	<div class="homefeedback-box">
	<?php print render($content['field_page_feedback_rows']);?> 
	</div>
	</div>	
<?php endif;?>

 <?php if(isset($content['field_page_video_rows'])):?>
 <div class="video-section">
<div class="width-1">
<?php print render($content['field_page_video_rows']);?> 
</div>
</div>
 <?php endif;?>

</div>