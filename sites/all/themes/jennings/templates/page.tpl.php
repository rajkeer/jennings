<header class="site-header" role="banner">
  <div id="header-inner">

    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo">Jennings</a><span class="site-slogan"><?php echo $site_slogan; ?></span>

    <?php print render($page['header']);?>
      <nav>
    <a id="navToggle" class="nav-toggle"><span></span></a>
      
    <?php print render($page['nav']);?>
  </nav>
  </div>

</header>

<?php if(isset($emergencyMessage)):?>
  <?php print $emergencyMessage; ?>
<?php endif;?>

<div id="wrapper" class="animated fadeIn">

  <?php $tab_rendered = render($tabs); ?>
  <?php if ($page['highlighted'] || $messages || !empty($tab_rendered) || $action_links): ?>
    <div class="">
      <div class="" id="admin-functions">
        <?php if ($page['highlighted']): ?>
          <div id="highlighted"><?php print render($page['highlighted']); ?></div>
        <?php endif; ?>
        <?php print $messages; ?>
        <?php if (!empty($tab_rendered)): ?><div class="tabs clearfix"><?php print $tab_rendered; ?></div><?php endif; ?>
        <?php print render($page['help']); ?>
        <?php if ($action_links): ?>
          <ul class="action-links hidden"><?php print render($action_links); ?></ul>
        <?php endif; ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="">
    <div class="">
      <?php print render($page['content']); ?>
    </div>
  </div>
  
  <?php if($page['footer_top']) { ?>
   <div class="footer_top-section">
     <?php print render($page['footer_top']); ?>
  </div> 
  <?php } ?>
  <footer class="site-footer" role="contentinfo">
    <div class="row">
      <div class="large-4 columns matchheight left">
          <?php print render($page['footer_left']); ?>
      </div>
      <div class="large-4 columns matchheight right">
          <?php print render($page['footer_right']); ?>
      </div>
    </div>
	<div class="row">
	   <div class="large-5 columns footer-bottom">
          <?php print render($page['footer_botton']); ?>
      </div>
	  </div>
  </footer>
</div>

<div class="hidden">
  <?php print render($page['hidden']); ?>
</div>

<?php if(isset($page['overlay_container'])):?>
  <?php print render($page['overlay_container']);?>
<?php endif;?>