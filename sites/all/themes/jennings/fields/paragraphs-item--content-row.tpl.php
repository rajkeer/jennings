<?php if(isset($content['field_image'])):?>
<?php $imageURL = file_create_url(($content['field_image'][0]['#item']['uri'])); ?> 
<div class="image-fold-box"><img src="<?php print $imageURL; ?>" width="100%" alt="" /></div>
<?php endif;?>
<?php if(isset($content['field_content'])):?>
<div class="container clearfix <?php print $classes; ?>">
    <div class="large-6 <?php print isset($content['field_content']['#additional_class'])?$content['field_content']['#additional_class']:''; ?>">
        <div class="image-fold-text">
            <h1><?php print render($content['field_heading_title']); ?></h1>
            <div class="descp">
                <?php print render($content['field_content']); ?>
            </div>
            <div class="img-fold-a"><?php print render($content['field_more_url']); ?></div>
        </div>
    </div>
</div>
<?php endif;?>
