<?php foreach ($items as $delta => $item) : $bgcolor='';?>
<?php foreach ($item['entity']['paragraphs_item'] as $key => &$subitem){
$subitem['field_content']['#additional_class']=$delta % 2 ? 'left' : 'right';
 $bgcolor = isset($subitem['field_backgroundcolor'])?render($subitem['field_backgroundcolor']):$bgcolor;
 break;
}
  ?>
<div class="<?php print $delta % 2 ? 'image-fold-right' : 'image-fold-left'; ?> fold-bg<?php print $delta+1; ?> image-fold <?php print trim(strip_tags($bgcolor)); ?>">
    <div class="container-fluid">
	<?php print render($item); ?>
    </div>
</div>
<?php endforeach; ?>