<?php if ($content['field_banner_image']) { ?>
    <?php $imageURL = file_create_url(($content['field_banner_image'][0]['#item']['uri'])); ?> 
    <div class="main-img banner-main">
        <img src="<?php print $imageURL; ?>" width="100%" alt="" />
        <?php if ($content['field_banner_title'] || $content['field_banner_desc']) { ?>
            <div class="banner-detail">
                <?php if ($content['field_banner_image']) { ?>
                    <div class="banner-sub"><?php print render($content['field_banner_title']); ?> </div>
                <?php } ?>
                <?php if ($content['field_banner_image']) { ?>
                    <div class="banner-desv"><?php print render($content['field_banner_desc']); ?>  </div> 
                <?php } ?>
            </div> 
        <?php } ?>
    </div>
<?php } ?>