<?php

/**
 * Changes the default meta content-type tag to the shorter HTML5 version
 */
function jennings_html_head_alter(&$head_elements) {
  $head_elements['system_meta_content_type']['#attributes'] = array(
    'charset' => 'utf-8'
  );
}


/**
 * Changes the search form to use the HTML5 "search" input attribute
 */
function jennings_preprocess_search_block_form(&$vars) {
  $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
  
}


/**
 * Hook_preprocess_html()
 */
function jennings_preprocess_html(&$vars) {

  // Enable swipebox
  if (theme_get_setting('swipebox')):
    drupal_add_js(path_to_theme() . '/js/libs/jquery.swipebox.min.js', array( 'weight' => 0));
    drupal_add_js(array('swipebox_enabled' => TRUE), 'setting');
    drupal_add_css(path_to_theme() . '/css/swipebox.css', array('group' => CSS_THEME, 'weight' => 1));
  endif;

  // Enable slick
  if (theme_get_setting('slick')):
    drupal_add_js(path_to_theme() . '/js/libs/slick.min.js', array( 'weight' => 0));
    drupal_add_js(array('slick_enabled' => TRUE), 'setting');
    drupal_add_css(path_to_theme() . '/css/slick.css', array('group' => CSS_THEME, 'weight' => 0));
  endif;

  // Enable overlay
  if (theme_get_setting('overlay')):
    drupal_add_js(path_to_theme() . '/js/jennings.overlay.js', array( 'weight' => 0));
    drupal_add_js(array('overlay_enabled' => TRUE), 'setting');
    drupal_add_css(path_to_theme() . '/css/overlay.css', array('group' => CSS_THEME, 'weight' => 0));
  endif;

  // Enable matchHeight
  if (theme_get_setting('matchHeight')):
    drupal_add_js(path_to_theme() . '/js/libs/jquery.matchHeight-min.js', array( 'weight' => 0));
    drupal_add_js(array('matchHeight_enabled' => TRUE), 'setting');
  endif;

  // If plugins reaquired, load init file
  if (theme_get_setting('swipebox') || theme_get_setting('slick') || theme_get_setting('matchHeight')) {
    drupal_add_js(path_to_theme() . '/js/jennings.plugins.js', array( 'weight' => 0));
  }

  // If home page
  if($vars['is_front']) {
    drupal_add_js(path_to_theme() . '/js/jennings.front.js');
  }
  
  // Ensure that the $vars['rdf'] variable is an object.
  if (!isset($vars['rdf']) || !is_object($vars['rdf'])) {
    $vars['rdf'] = new StdClass();
  }

  if (module_exists('rdf')) {
    $vars['doctype'] = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML+RDFa 1.1//EN">' . "\n";
    $vars['rdf']->version = 'version="HTML+RDFa 1.1"';
    $vars['rdf']->namespaces = $vars['rdf_namespaces'];
    $vars['rdf']->profile = ' profile="' . $vars['grddl_profile'] . '"';
  } else {
    $vars['doctype'] = '<!DOCTYPE html>' . "\n";
    $vars['rdf']->version = '';
    $vars['rdf']->namespaces = '';
    $vars['rdf']->profile = '';
  }
 
}

/**
 * Returns HTML for a list or nested list of items.
 *
 * This is a clone of the core theme_item_list() function with the div class="item-list" removed.
 */
function jennings_item_list($variables) {
  $items = $variables['items'];
  $title = $variables['title'];
  $type = $variables['type'];
  $attributes = $variables['attributes'];

  $output = '';
  if (isset($title)) {
    $output .= '<h3>' . $title . '</h3>';
  }

  if (!empty($items)) {
    $output .= "<$type" . drupal_attributes($attributes) . '>';
    $num_items = count($items);
    foreach ($items as $i => $item) {
      $attributes = array();
      $children = array();
      $data = '';
      if (is_array($item)) {
        foreach ($item as $key => $value) {
          if ($key == 'data') {
            $data = $value;
          }
          elseif ($key == 'children') {
            $children = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $data = $item;
      }
      if (count($children) > 0) {
        // Render nested list.
        $data .= theme_item_list(array('items' => $children, 'title' => NULL, 'type' => $type, 'attributes' => $attributes));
      }
      if ($i == 0) {
        $attributes['class'][] = 'first';
      }
      if ($i == $num_items - 1) {
        $attributes['class'][] = 'last';
      }
      $output .= '<li' . drupal_attributes($attributes) . '>' . $data . "</li>\n";
    }
    $output .= "</$type>";
  }
  return $output;
}


/**
 * Process variables for menu-block-wrapper.tpl.php.
 *
 * @see menu-block-wrapper.tpl.php
 */
function jennings_preprocess_menu_block_wrapper(&$variables) {
  $variables['classes_array'][] = 'menu-block-' . $variables['delta'];
  $variables['classes_array'][] = 'menu-name-' . $variables['config']['menu_name'];
  $variables['classes_array'][] = 'parent-mlid-' . $variables['config']['parent_mlid'];
  $variables['classes_array'][] = 'menu-level-' . $variables['config']['level'];
}


/**
* Implementation of hook_preprocess_page().
* Different page.tpl.php for panels pages.
*/
function jennings_preprocess_page(&$vars) {
  // if this is a panel page, add template suggestions

  // page.tpl per content type
  if (isset($vars['node']->type)) { 
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type; 
  }

  // Overlay
  if (theme_get_setting('overlay')):
    $vars['page']['overlay_container'] = '<div id="olContainer" class="overlay-wrapper"></div>';
  endif;
  
  // Copyright text
  if (theme_get_setting('copyright')):
    $year = date('Y');
    $vars['page']['custom_copyright'] = '&copy; '. $year . ' ' . theme_get_setting('copyright');
  endif;

   $vars['site_slogan'] = variable_get('site_slogan', 'a home for your business');

  // Emergency message
  $emBlock = block_load('bean', 'emergency-message');
  $em_render_array = _block_get_renderable_array(_block_render_blocks(array($emBlock)));
  $vars['emergencyMessage'] = render($em_render_array);
}


/**
* Implementation of hook_preprocess_block().
*/
function jennings_preprocess_block(&$variables) {
  // tpl for Emergency message blocks
  if ($variables['block']->module == 'bean' && $variables['block']->delta == 'emergency-message') {    
    $variables['theme_hook_suggestions'][] = 'block__bean__emergency_message';
  }
}


/**
* Customise Main Menu output.
*/
function jennings_links__system_main_menu($variables) {

  $html = '<ul class="main-menu">'; 

  foreach ($variables['links'] as $link) {
    $classes = '';
    if(isset($link['attributes']['class'])){
      foreach($link['attributes']['class'] as $key => $class) {
        $classes .= $class . ' ';
      }
    }
    $path = drupal_get_path_alias($link['href']);
    $html .= '<li><a class="'. $classes . '" href="/'.$path.'">'.$link['title'].'</a></li>';
  }

  $html .= '</ul>';

  return $html;
}

/**
* Implementation of hook_form_alter().
*/
function jennings_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form'){
    $form['search_block_form']['#attributes']['placeholder'] = t( 'Search >' );
  }
  if ($form_id == 'webform_client_form_21'){ 
    $form['actions']['submit']['#value'] = t( 'Submit >' );
  }
  
}



